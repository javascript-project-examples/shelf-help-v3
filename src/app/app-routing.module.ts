import { RouterModule, Routes } from "@angular/router";
import { NgModule } from "@angular/core"
import { BookPage } from "./pages/book/book.page";
import { LoginPage } from "./pages/login/login.page";
import { NotesPage } from "./pages/notes/notes.page";
import { AuthGuard } from "./guards/auth.guard";
import { LoggedGuard } from "./guards/logged.guard";

const routes: Routes = [
    {
        path: '', component: BookPage
    },
    {
        path: 'login', component: LoginPage, canActivate:[LoggedGuard]
    },
    {
        path: 'notes/:bookId', component: NotesPage, canActivate:[AuthGuard]
    }
]

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule {

}