import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from 'src/app/services/user.service';
import {NgForm} from "@angular/forms"
import { User } from 'src/app/models/user';
import keycloak from 'src/keycloak';

@Component({
  selector: 'app-login-form',
  templateUrl: './login-form.component.html',
  styleUrls: ['./login-form.component.css']
})
export class LoginFormComponent implements OnInit {

  constructor(private readonly userService: UserService, private readonly router:Router) { }


  ngOnInit(): void {
    this.userService.JokeSwitch()
  }

  login(form:NgForm) {
console.log(form.value)
keycloak.login()
    // localStorage.setItem("username",form.value.username)
    // let currentUser:User={
    //   userName:form.value.username,
    //   password:form.value.password
    // }
    // this.userService.User = currentUser

 //this.router.navigateByUrl("")
  }
  showToken()
  {
      console.log(keycloak.token)
      console.log(keycloak.tokenParsed)
      
  }
  
  logout(){
    keycloak.logout()
  }
}


