import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Book } from 'src/app/models/book';

@Component({
  selector: 'app-book-list-item',
  templateUrl: './book-list-item.component.html',
  styleUrls: ['./book-list-item.component.css']
})
export class BookListItemComponent implements OnInit {
@Input() bookParam? :Book
@Output() bookClicked:EventEmitter<number> = new EventEmitter();

  constructor() { }
  imgSrc:string =""

  ngOnInit(): void {
    this.imgSrc = `https://thoughtful-vagabond-fibre.glitch.me/${this.bookParam?.coverImg}`
  }

 clickedBook(){
    // notify parent
    this.bookClicked.emit(this.bookParam?.id)
  }

}
