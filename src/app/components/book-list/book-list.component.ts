
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Book } from 'src/app/models/book';
import { User } from 'src/app/models/user';
import { BookService } from 'src/app/services/book.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-book-list',
  templateUrl: './book-list.component.html',
  styleUrls: ['./book-list.component.css']
})
export class BookListComponent implements OnInit {

  public books: Book[] = []



  constructor(private readonly bookService: BookService, private readonly userService:UserService, private readonly router:Router ) {

  }

 get user():User{
  return this.userService.User
 }

 handleBookClicked(data:number){
  this.router.navigateByUrl(`/notes/${data}`)
 }

  ngOnInit(): void {
    // get book data
    this.bookService.getBooks().subscribe({
      next: (booksData) => {
        this.books = booksData
      },
      error: (error) => console.log(error)
    })
  }

}
