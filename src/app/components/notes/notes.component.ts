import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Book } from 'src/app/models/book';
import { User } from 'src/app/models/user';
import { BookService } from 'src/app/services/book.service';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-notes',
  templateUrl: './notes.component.html',
  styleUrls: ['./notes.component.css']
})
export class NotesComponent implements OnInit {

  currentBook?:Book

  constructor(private readonly userService: UserService, private readonly activeRoute: ActivatedRoute, private readonly bookService: BookService) { }

  ngOnInit(): void {


    this.bookService.getBookById(Number(this.activeRoute.snapshot.paramMap.get('bookId')))
    .subscribe({
      next:(book=>this.currentBook=book),
      error:error=>console.log(error
        )
    })

  }

  get user(): User {
    return this.userService.User
  }

}
