import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Book } from "../models/book";
import { Observable } from "rxjs";

@Injectable({ providedIn: "root" })
export class BookService {

    constructor(private readonly httpClient: HttpClient) {

    }

    getBooks(): Observable<Book[]> {
        return this.httpClient.get<Book[]>('https://thoughtful-vagabond-fibre.glitch.me/books')

    }

    getBookById(bookId:number): Observable<Book> {
        return this.httpClient.get<Book>(`https://thoughtful-vagabond-fibre.glitch.me/books/${bookId}`)

    }


}