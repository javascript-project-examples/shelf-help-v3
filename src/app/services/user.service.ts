import { Injectable } from '@angular/core';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs';
import {switchMap} from 'rxjs/operators'

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private readonly httpClient: HttpClient) { }

  // private User
  private user: User = {
    userName: "unknown",
    password: "none"
  }

  // getter
  get User(): User {
    return this.user
  }

  set User(newUser:User) {
    this.user = newUser
  }

  JokeSwitch():void{
      this.httpClient.get<any>("https://randomuser.me/api/")
        .pipe(
         // change to different observable
        
            switchMap(originalData => {

               return this.httpClient.get<any>("https://v2.jokeapi.dev/joke/Any?blacklistFlags=nsfw,religious,political,racist,sexist,explicit")
            })
        )
        .subscribe({
          next: (joke: any) => {
           console.log(joke)
          },
          error: error => console.log(error)
        })
  }
  // set up

  getSampleUser(): void {
    this.httpClient.get<any>("https://randomuser.me/api/")
      .pipe(
        map(userSampleData => {
          let newUser: User = {
            userName: userSampleData.results[0].name.first,
            password: userSampleData.results[0].picture.medium
          }

          return newUser
        })

      )
      .subscribe({
        next: (userData: User) => {
          this.user = userData 
        },
        error: error => console.log(error)
      })


  }

}
